package com.example.exampleproject.configuration.jwt;

import com.example.exampleproject.security.UserServiceDetails;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private UserServiceDetails userService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Value("${jwt.start}")
    private String startKey;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String token = httpServletRequest.getHeader("Authorization");
        String email =null;
        String jwtToken = null;

        if(httpServletRequest.getSession().getAttribute("TOKEN") != null)
            token = httpServletRequest.getSession().getAttribute("TOKEN").toString();

        //jwt Token is in the form "Bearer token". Remove Bearer word and get only the Token
        if(token!=null && token.startsWith(startKey) ){
            jwtToken = StringUtils.removeStart(token,startKey);
            try{
                email=jwtTokenUtil.getUsernameFromToken(jwtToken);
            }catch (IllegalArgumentException e){
                System.out.println("Unable to get JWT Token");

            }catch (ExpiredJwtException e){
                System.out.println("JWT Token has Expired");
            }
        }else {
            logger.warn("JWT token does not begin with "+ startKey+" String");
        }

        //Once we get the token validate it
        if(email!=null&& SecurityContextHolder.getContext().getAuthentication()==null ){
            UserDetails userDetails = userService.loadUserByUsername(email);

            //if token is valid configure spring security to manually set authentication
            if(jwtTokenUtil.validateToken(jwtToken,userDetails)){

                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));

                //After setting the authentication in the context , we specify that the current user is authenticated. so it passes the
                //spring security Configurations successfully.
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);


            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}

