package com.example.exampleproject.configuration;


import com.example.exampleproject.component.jwtConfig.CAccessDeniedHandler;
import com.example.exampleproject.component.jwtConfig.CAuthSuccessHandler;
import com.example.exampleproject.component.jwtConfig.CUnauthorizedHandler;
import com.example.exampleproject.configuration.jwt.JwtRequestFilter;
import com.example.exampleproject.security.UserServiceDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig  {

    @Autowired
    UserServiceDetails userService;

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * Config public url
     */
    @Configuration
    public static class AnonymousConfigurationAdapter extends WebSecurityConfigurerAdapter {

        private String[] url={
                "/css/**",
                "/js/**",
                "/image/**",
                "*.html",
                "/swagger-ui.html"
                };

        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring().antMatchers(url);
        }

    }

    @Configuration
    @Order(2)
    public static class UserApiConfig extends WebSecurityConfigurerAdapter{

        @Autowired
        private com.example.exampleproject.component.jwtConfig.CUnauthorizedHandler CUnauthorizedHandler;

        @Autowired
        private CAccessDeniedHandler accessDeniedHandler;

        @Autowired
        private JwtRequestFilter jwtRequestFilter;

        @Autowired
        private UserServiceDetails userService;

        @Autowired
        private PasswordEncoder passwordEncoder;


        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
        }

        @Bean
        @Override
        public AuthenticationManager authenticationManagerBean() throws Exception {
            return super.authenticationManagerBean();
        }


        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable().requestMatchers()
                    .antMatchers("/api/**")
                    .and()
                    .authorizeRequests()
                    .antMatchers("/api/v1/login").permitAll()
                    .antMatchers("/api/user").hasAnyRole("USER","ADMIN")
                    .antMatchers("/api/admin").hasRole("ADMIN")
                    .anyRequest()
                    .authenticated()
                    .and()
                    .exceptionHandling()
                    .accessDeniedHandler(accessDeniedHandler)
                    .authenticationEntryPoint(CUnauthorizedHandler)
                    .and()
                    .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
            http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

        }
    }

    @Configuration
    @Order(3)
    public static class WebConfig extends WebSecurityConfigurerAdapter{

        @Autowired
        private CAuthSuccessHandler authSuccessHandler;

        @Autowired
        private UserServiceDetails userService;

        @Autowired
        private PasswordEncoder passwordEncoder;

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {

            http
                    .authorizeRequests()
                    .antMatchers("/admin/**").hasRole("ADMIN")
                    .antMatchers("/swagger-ui.html","/swagger-ui/**","/v3/api-docs").permitAll()
                    .anyRequest()
                    .authenticated()
                    .and()
                    .formLogin()
                    //web
                    .loginPage("/custom_login")
                    //html
                    .loginProcessingUrl("/custom_login")
                    .defaultSuccessUrl("/",true)
                    .successHandler(authSuccessHandler)
                    .failureUrl("/login?error=true")
                    .permitAll()
                    .and()
                    .csrf()
                    .disable()
                    .logout()
                    .logoutUrl("/logout")
                    .permitAll();
        }
    }


}
