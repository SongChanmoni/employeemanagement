package com.example.exampleproject.service.teacher;


import com.example.exampleproject.domain.teacher.Teacher;
import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.pagination.PagingMainResponse;
import com.example.exampleproject.payload.teacher.TeacherRequest;
import org.springdoc.core.converters.models.Pageable;

import java.util.List;

public interface TeacherService {

    ResponseData<?> getAll();

    PagingMainResponse<?> getAllWithPaging(Pageable pageable);

    ResponseData<?> getOne(Long id) throws Throwable;

    Teacher insert(TeacherRequest teacherRequest) throws Throwable;

    void update(TeacherRequest teacherRequest,Long id) throws Throwable;

    void delete(Long id) throws Throwable;

    List<Teacher> findTeacherByName(String name);

}
