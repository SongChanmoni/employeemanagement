package com.example.exampleproject.service.teacher;


import com.example.exampleproject.domain.quiz.Quiz;
import com.example.exampleproject.domain.quiz.QuizRepository;
import com.example.exampleproject.domain.teacher.Teacher;
import com.example.exampleproject.domain.teacher.TeacherRepository;
import com.example.exampleproject.enums.ErrorCode;
import com.example.exampleproject.exception.BusinessException;
import com.example.exampleproject.exception.EntityNotFoundException;
import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.pagination.PagingMainResponse;
import com.example.exampleproject.payload.teacher.TeacherRequest;
import com.example.exampleproject.payload.teacher.TeacherResponse;
import org.springdoc.core.converters.models.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service(value = "TeacherService")
@Transactional
public class TeacherServiceImpl implements TeacherService{

    private TeacherRepository teacherRepository;
    private QuizRepository quizRepository;

    @Autowired
    public void setQuizRepository(QuizRepository quizRepository) {
        this.quizRepository = quizRepository;
    }

    @Autowired
    void setTeacherRepository(TeacherRepository teacherRepository)
    {
        this.teacherRepository=teacherRepository;
    }


    //get all teacher
    @Override
    public ResponseData<?> getAll() {
        List<Teacher> teacherList = teacherRepository.findAll();
        return new ResponseData<>(teacherList);
    }

    //get teacher with paging
    @Override
    public PagingMainResponse<?> getAllWithPaging(Pageable pageable) {
        Page<Teacher> teacherPage = teacherRepository.findAll((org.springframework.data.domain.Pageable) pageable);
        return new PagingMainResponse<>(teacherPage,teacherPage.getContent());
    }

    //get teacher by id
    @Override
    public ResponseData<?> getOne(Long id) throws Throwable {
     Teacher teacher = teacherRepository.findById(id).orElseThrow(() ->{
         throw new EntityNotFoundException(Teacher.class,"id",id.toString());
     });
        TeacherResponse teacherResponse = TeacherResponse.builder()
                .name(teacher.getName())
                .gender(teacher.getGender())
                .build();
        return new ResponseData<>(teacherResponse);
    }

    //insert teacher
    @Override
    public Teacher insert(TeacherRequest teacherRequest) throws Throwable {
        Teacher teacher = teacherRequest.toModel();
        try {
            teacher = teacherRepository.saveAndFlush(teacher);
            return teacher;
        }catch (Exception exception){
            exception.printStackTrace();
            throw new BusinessException(ErrorCode.BAD_REQUEST,exception.getMessage());

        }
    }

    //update teacher
    @Override
    public void update(TeacherRequest teacherRequest, Long id) throws Throwable {

        Teacher teacher = teacherRepository.findById(id).orElseThrow(() ->{
            throw new EntityNotFoundException(Teacher.class,"id",id.toString());
        });
        teacher.setId(teacherRequest.toModel().getId());
        teacher.setName(teacherRequest.getName());
        teacher.setGender(teacherRequest.getGender());
        teacherRepository.save(teacher);
    }

    //delete teacher
    @Override
    public void delete(Long id) throws Throwable {
        try{
            Teacher teacher = teacherRepository.findById(id).orElseThrow(() ->{
                throw new EntityNotFoundException(Teacher.class,"id",id.toString());
            });
            teacherRepository.delete(teacher);
        }catch (Exception exception){
            exception.printStackTrace();
            throw new BusinessException(ErrorCode.ITEM_NOT_FOUND,exception.getMessage());
        }
    }

    //filter teacher
    @Override
    public List<Teacher> findTeacherByName(String name) {
        if (name != null){
            return teacherRepository.findTeacherByName(name);
        }
        return teacherRepository.findTeacherByName(name);
    }
}
