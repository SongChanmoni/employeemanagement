package com.example.exampleproject.service.employee;


import com.example.exampleproject.domain.employee.Employee;
import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.employee.EmployeeRequest;
import com.example.exampleproject.payload.pagination.PagingMainResponse;
import org.springdoc.core.converters.models.Pageable;
import org.springframework.data.domain.Page;

import java.util.List;

public interface EmployeeService {

    List<Employee> getAllEmployees();
    void saveEmployee(Employee employee);
    Employee getEmployeeById(long id);
    void deleteEmployeeById(long id);

    Page< Employee > findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);
}
