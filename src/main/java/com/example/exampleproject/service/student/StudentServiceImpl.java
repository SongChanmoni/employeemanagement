package com.example.exampleproject.service.student;

import com.example.exampleproject.domain.classdomain.Class;
import com.example.exampleproject.domain.classdomain.ClassRepository;
import com.example.exampleproject.domain.student.Student;
import com.example.exampleproject.domain.student.StudentRepository;
import com.example.exampleproject.enums.ErrorCode;
import com.example.exampleproject.exception.BusinessException;
import com.example.exampleproject.exception.EntityNotFoundException;
import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.pagination.PagingMainResponse;
import com.example.exampleproject.payload.student.StudentRequest;
import com.example.exampleproject.payload.student.StudentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service(value = "StudentService")
@Transactional
public class StudentServiceImpl implements StudentService {

    private StudentRepository studentRepository;
    private ClassRepository classRepository;

    @Autowired
    public void setClassRepository(ClassRepository classRepository) {this.classRepository = classRepository;}

    @Autowired
    void setStudentRepository(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }

    //get all student
    @Override
    public ResponseData<?> getAll() {
        List<Student> studentList = studentRepository.findAll();
       return  new ResponseData<>(studentList);
    }

    //get student with paging
    @Override
    public PagingMainResponse<?> getAllWithPaging(Pageable pageable) {

        Page<Student> studentPage = studentRepository.findAll(pageable);
        return new PagingMainResponse<>(studentPage,studentPage.getContent());
    }

    //get student by id
    @Override
    public ResponseData<?> getOne(Long id) throws Throwable {
        Student student = studentRepository.findById(id).orElseThrow(() ->{
            throw new EntityNotFoundException(Student.class,"id",id.toString());
        });
        StudentResponse studentResponse = StudentResponse.builder()
                .id(id)
                .name(student.getName())
                .gender(student.getGender())
                .build();

        return new ResponseData<>(studentResponse);
    }

    //insert student
    @Override
    public Student insert(StudentRequest studentRequest) throws Throwable {
        Student student = studentRequest.toModel();

        try {
            student = studentRepository.saveAndFlush(student);
            return student;

        }catch (Exception exception){
            exception.printStackTrace();
            throw  new BusinessException(ErrorCode.BAD_REQUEST,exception.getMessage());
        }
    }

    //update student
    @Override
    public void update(StudentRequest studentRequest, Long id) throws Throwable {
        Student student = studentRepository.findById(id).orElseThrow(() ->{
            throw new EntityNotFoundException(Student.class,"id",id.toString());
        });

        student.setName(studentRequest.getName());
        student.setGender(studentRequest.getGender());

    }

    //delete student
    @Override
    public  void delete(Long id) throws Throwable {

        try {
            Student student = studentRepository.findById(id).orElseThrow(() -> {
                throw  new EntityNotFoundException(Student.class,"id",id.toString());
            });
            studentRepository.delete(student);

        }catch (Exception exception){
            exception.printStackTrace();
            throw new BusinessException(ErrorCode.ITEM_NOT_FOUND,exception.getMessage());

        }


    }

    //filter student
    @Override
    public List<Student> findStudentByName(String name) {
        if (name != null){
            return studentRepository.findStudentByName(name);
        }
        return studentRepository.findStudentByName(name);
    }


}
