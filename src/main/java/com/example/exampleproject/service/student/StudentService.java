package com.example.exampleproject.service.student;


import com.example.exampleproject.domain.student.Student;
import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.pagination.PagingMainResponse;
import com.example.exampleproject.payload.student.StudentRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface StudentService {

    ResponseData<?> getAll();

    PagingMainResponse<?> getAllWithPaging(Pageable pageable);

    ResponseData<?> getOne(Long id) throws Throwable;

    Student insert(StudentRequest studentRequest) throws Throwable;

    void update(StudentRequest studentRequest,Long id) throws Throwable;

    void delete(Long id) throws Throwable;

    List<Student> findStudentByName(String name);


}
