package com.example.exampleproject.service.user;

import com.example.exampleproject.domain.user.User;
import com.example.exampleproject.domain.user.UserRepository;
import com.example.exampleproject.enums.ErrorCode;
import com.example.exampleproject.exception.BusinessException;
import com.example.exampleproject.exception.EntityNotFoundException;
import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.pagination.PagingMainResponse;
import com.example.exampleproject.payload.user.UserRequest;
import com.example.exampleproject.payload.user.UserResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService{

    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    //get all user
    @Override
    public ResponseData<?> getAll() {
        List<User> user = userRepository.findAll();
        return new ResponseData<>(user);
    }

    //get user with paging
    @Override
    public PagingMainResponse<?> getAllWithPaging(Pageable pageable) {
        Page<User> users = userRepository.findAll(pageable);
        return new PagingMainResponse<>(users, users.getContent());
    }

    //get  user by id
    @Override
    public ResponseData<?> getOne(Long id) throws Throwable {
        User user = userRepository.findById(id).orElseThrow(() -> {
            throw new EntityNotFoundException(User.class, "id", id.toString());
        });

        UserResponse response = UserResponse.builder()
                .firstname(user.getFirstname())
                .lastname(user.getLastname())
                .username(user.getUsername())
                .build();
        return new ResponseData<>(user);
    }

    //insert user
    @Override
    public User insert(UserRequest userRequest) throws Throwable {
        User user = userRequest.toModel();
        user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        try {
            user = userRepository.saveAndFlush(user);
            return user;
        }catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(ErrorCode.BAD_REQUEST, e.getMessage());
        }
    }

    //update user
    @Override
    public void update(UserRequest userRequest, Long id) throws Throwable {

        User user = userRepository.findById(id).orElseThrow(()->{
            throw new EntityNotFoundException(User.class, "id", id.toString());
        });

        user.setFirstname(userRequest.getFirstname());
        user.setLastname(userRequest.getLastname());
        //user.setPhoneNumber(userRequest.getPhoneNumber());
    }

    //delete user
    @Override
    public void delete(Long id) throws Throwable {
        try {

            User user = userRepository.findById(id).orElseThrow(() -> {
                throw new EntityNotFoundException(User.class, "id", id.toString());
            });
            userRepository.delete(user);

        }catch (Exception exception){
            exception.printStackTrace();
            throw new BusinessException(ErrorCode.BAD_REQUEST,exception.getMessage());
        }

    }



//    //filter user
//    @Override
//    public List<User> listAll(String keyword) {
//        if(keyword != null ){
//            return userRepository.search(keyword);
//        }
//        return userRepository.search(keyword);
//    }

}
