package com.example.exampleproject.service.user;

import com.example.exampleproject.domain.user.User;
import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.pagination.PagingMainResponse;
import com.example.exampleproject.payload.user.UserRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {

    ResponseData<?> getAll();

    PagingMainResponse<?> getAllWithPaging(Pageable pageable);

    ResponseData<?> getOne(Long id) throws Throwable;

    User insert(UserRequest userRequest) throws Throwable;

    void update(UserRequest userRequest, Long id) throws Throwable;

    void delete(Long id) throws Throwable;

//    List<User> listAll(String keyword);

}
