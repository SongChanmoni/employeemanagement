package com.example.exampleproject.service.classservice;


import com.example.exampleproject.domain.classdomain.Class;
import com.example.exampleproject.domain.classdomain.ClassRepository;
import com.example.exampleproject.domain.student.Student;
import com.example.exampleproject.domain.student.StudentRepository;
import com.example.exampleproject.domain.teacher.Teacher;
import com.example.exampleproject.domain.teacher.TeacherRepository;
import com.example.exampleproject.enums.ErrorCode;
import com.example.exampleproject.exception.BusinessException;
import com.example.exampleproject.exception.EntityNotFoundException;
import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.classpayload.ClassRequest;
import com.example.exampleproject.payload.classpayload.ClassRespose;
import com.example.exampleproject.payload.pagination.PagingMainResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassServiceImpl implements ClassService{

    @Autowired
    private ClassRepository classRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TeacherRepository  teacherRepository;


    //get all class
    @Override
    public ResponseData<?> getAll() {
        List<Class> classList = classRepository.findAll();
        return new ResponseData<>(classList);
    }

    //get class with paging
    @Override
    public PagingMainResponse<?> getAllWithPaging(Pageable pageable) {
        Page<Class> classPage = classRepository.findAll(pageable);
        return new PagingMainResponse<>(classPage,classPage.getContent());
    }

    //get class by id
    @Override
    public ResponseData<?> getOne(Long id) throws Throwable {
        Class class1 = classRepository.findById(id).orElseThrow(() ->{
            throw new EntityNotFoundException(Class.class,"id",id.toString());
        });

        ClassRespose classRespose = ClassRespose.builder()
                .id(id)
                .name(class1.getName())
                .build();

        return new ResponseData<>(classRespose);
    }

    //insert class
    @Override
    public Class insert(ClassRequest classRequest) throws Throwable {
        Class class2 = classRequest.toModel();
        List<Student> student = studentRepository.findAllById(classRequest.getStudentId());
        class2.setStudents(student);
        try{
            class2 = classRepository.saveAndFlush(class2);
            return class2;
        }catch (Exception exception){
            exception.printStackTrace();
            throw new BusinessException(ErrorCode.BAD_REQUEST,exception.getMessage());
        }
    }

    //update class
    @Override
    public void update(ClassRequest classRequest, Long id) throws Throwable {
        Class aClass = classRepository.findById(id).orElseThrow(() ->{
            throw new EntityNotFoundException(Class.class,"id",id.toString());
        });
        aClass.setName(classRequest.getName());
        classRepository.save(aClass);
    }

    //delete class
    @Override
    public void delete(Long id) throws Throwable {

        try {
            Class aClass= classRepository.findById(id).orElseThrow(() -> {
                throw  new EntityNotFoundException(Class.class,"id",id.toString());
            });
            classRepository.delete(aClass);

        }catch (Exception exception){
            exception.printStackTrace();
            throw new BusinessException(ErrorCode.ITEM_NOT_FOUND,exception.getMessage());

        }

    }

    //filter class
    @Override
    public List<Class> findClassByName(String name) {
        if (name != name){
            return classRepository.findClassByName(name);
        }
        return classRepository.findClassByName(name);
    }
}
