package com.example.exampleproject.service.classservice;


import com.example.exampleproject.domain.classdomain.Class;
import com.example.exampleproject.domain.student.Student;
import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.classpayload.ClassRequest;
import com.example.exampleproject.payload.pagination.PagingMainResponse;
import com.example.exampleproject.payload.student.StudentRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ClassService {

    ResponseData<?> getAll();

    PagingMainResponse<?> getAllWithPaging(Pageable pageable);

    ResponseData<?> getOne(Long id) throws Throwable;

    Class insert(ClassRequest classRequest) throws Throwable;

    void update(ClassRequest classRequest,Long id) throws Throwable;

    void delete(Long id) throws Throwable;

    List<Class> findClassByName(String name);

}
