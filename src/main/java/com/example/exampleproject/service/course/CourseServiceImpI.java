package com.example.exampleproject.service.course;


import com.example.exampleproject.domain.course.Course;
import com.example.exampleproject.domain.course.CourseRepository;
import com.example.exampleproject.domain.quiz.Quiz;
import com.example.exampleproject.domain.quiz.QuizRepository;
import com.example.exampleproject.domain.student.Student;
import com.example.exampleproject.domain.student.StudentRepository;
import com.example.exampleproject.domain.teacher.Teacher;
import com.example.exampleproject.domain.teacher.TeacherRepository;
import com.example.exampleproject.enums.ErrorCode;
import com.example.exampleproject.exception.BusinessException;
import com.example.exampleproject.exception.EntityNotFoundException;
import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.course.CourseRequest;
import com.example.exampleproject.payload.course.CourseResponse;
import com.example.exampleproject.payload.pagination.PagingMainResponse;
import org.springdoc.core.converters.models.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseServiceImpI implements CourseService{

    private CourseRepository courseRepository;
    private TeacherRepository teacherRepository;
    private StudentRepository studentRepository;
    private QuizRepository quizRepository;

    @Autowired
    public void setQuizRepository(QuizRepository quizRepository) {this.quizRepository = quizRepository;}

    @Autowired
    public void setStudentRepository(StudentRepository studentRepository) {this.studentRepository = studentRepository;}

    @Autowired
    public void setTeacherRepository(TeacherRepository teacherRepository) {this.teacherRepository = teacherRepository;}

    @Autowired
    public void setCourseRepository(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }


    //get all course
    @Override
    public ResponseData<?> getAll() {
        List<Course> courseList = courseRepository.findAll();
        return new ResponseData<>(courseList);
    }

    //get course with paging
    @Override
    public PagingMainResponse<?> getAllWithPaging(Pageable pageable) {
        Page<Course>  coursePage = courseRepository.findAll((org.springframework.data.domain.Pageable) pageable);
        return new PagingMainResponse<>(coursePage,coursePage.getContent());
    }

    //get course by id
    @Override
    public ResponseData<?> getOne(Long id) throws Throwable {
        Course course = courseRepository.findById(id).orElseThrow(() ->{
            throw  new EntityNotFoundException(Course.class,"id",id.toString());
        });
        CourseResponse courseResponse = CourseResponse.builder()
                .title(course.getTitle())
                .build();

        return new ResponseData<>(courseResponse);
    }

    //insert course
    @Override
    public Course insert(CourseRequest courseRequest) throws Throwable {
        Course course = courseRequest.toModel();
        Teacher teacher = teacherRepository.findById(courseRequest.getTeacherId()).get();
        List<Student> studentList = studentRepository.findAllById(courseRequest.getStudentId());
        List<Quiz> quizzes = quizRepository.findAllById(courseRequest.getQuizId());
        course.setTeacher(teacher);
        course.setStudents(studentList);
        course.setQuizzes(quizzes);

        try{
            course = courseRepository.saveAndFlush(course);
            return course;
        }catch (Exception exception){
            exception.printStackTrace();
            throw new BusinessException(ErrorCode.BAD_REQUEST,exception.getMessage());
        }
    }

    //update course
    @Override
    public void update(CourseRequest courseRequest, Long id) throws Throwable {

        Course course = courseRepository.findById(id).orElseThrow(() ->{
            throw new EntityNotFoundException(Course.class,"id",id.toString());
        });

        course.setTitle(courseRequest.getTitle());
        courseRepository.save(course);
    }

    //delete course
    @Override
    public void delete(Long id) throws Throwable {
        try {
            Course course = courseRepository.findById(id).orElseThrow(() ->{
                throw new EntityNotFoundException(Course.class,"id",id.toString());
            });
            courseRepository.delete(course);

        }catch (Exception exception){
            exception.printStackTrace();
            throw new BusinessException(ErrorCode.ITEM_NOT_FOUND,exception.getMessage());

        }
    }

    //filter course
    @Override
    public List<Course> findCourseByTitle(String title) {
        if (title != null){
            return courseRepository.findCourseByTitle(title);
        }
        return courseRepository.findCourseByTitle(title);
    }
}
