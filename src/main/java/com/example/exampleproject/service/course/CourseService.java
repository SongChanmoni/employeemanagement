package com.example.exampleproject.service.course;


import com.example.exampleproject.domain.course.Course;
import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.course.CourseRequest;
import com.example.exampleproject.payload.pagination.PagingMainResponse;
import org.springdoc.core.converters.models.Pageable;

import java.util.List;

public interface CourseService {

    ResponseData<?> getAll();

    PagingMainResponse<?> getAllWithPaging(Pageable pageable);

    ResponseData<?> getOne(Long id) throws  Throwable;

    Course insert(CourseRequest courseRequest) throws  Throwable;

    void update(CourseRequest courseRequest,Long id) throws Throwable;

    void delete(Long id) throws Throwable;

    List<Course> findCourseByTitle(String title);



}
