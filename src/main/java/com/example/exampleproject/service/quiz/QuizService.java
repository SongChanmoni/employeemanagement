package com.example.exampleproject.service.quiz;


import com.example.exampleproject.domain.quiz.Quiz;
import com.example.exampleproject.domain.user.User;
import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.pagination.PagingMainResponse;
import com.example.exampleproject.payload.quiz.QuizRequest;
import com.example.exampleproject.payload.user.UserRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface QuizService {
    ResponseData<?> getAll();

    PagingMainResponse<?> getAllWithPaging(Pageable pageable);

    ResponseData<?> getOne(Long id) throws Throwable;

    Quiz insert(QuizRequest quizRequest) throws Throwable;

    void update(QuizRequest quizRequest,Long id) throws Throwable;

    void delete(Long id) throws Throwable;

}
