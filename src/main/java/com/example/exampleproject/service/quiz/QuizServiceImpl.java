package com.example.exampleproject.service.quiz;

import com.example.exampleproject.domain.quiz.Quiz;
import com.example.exampleproject.domain.quiz.QuizRepository;
import com.example.exampleproject.enums.ErrorCode;
import com.example.exampleproject.exception.BusinessException;
import com.example.exampleproject.exception.EntityNotFoundException;
import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.pagination.PagingMainResponse;
import com.example.exampleproject.payload.quiz.QuizRequest;
import com.example.exampleproject.payload.quiz.QuizResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuizServiceImpl implements QuizService{

    @Autowired
    private QuizRepository quizRepository;

    @Override
    public ResponseData<?> getAll() {
        List<Quiz> quizzes = quizRepository.findAll();
        return new ResponseData<>(quizzes);
    }

    @Override
    public PagingMainResponse<?> getAllWithPaging(Pageable pageable) {
       Page<Quiz> quizPage = quizRepository.findAll(pageable);
       return new PagingMainResponse<>(quizPage,quizPage.getContent());
    }

    @Override
    public ResponseData<?> getOne(Long id) throws Throwable {
        Quiz quiz = quizRepository.findById(id).orElseThrow(() ->{
            throw new EntityNotFoundException(Quiz.class,"id",id.toString());
        });
        QuizResponse quizResponse = QuizResponse.builder()
                .id(quiz.getId())
                .title(quiz.getTitle())
                .question(quiz.getQuestion())
                .build();
        return new ResponseData<>(quizResponse);
    }

    @Override
    public Quiz insert(QuizRequest quizRequest) throws Throwable {
        Quiz quiz = quizRequest.toModel();
        try{
            quiz = quizRepository.saveAndFlush(quiz);
            return quiz;
        }catch (Exception exception){
            exception.printStackTrace();
            throw new BusinessException(ErrorCode.BAD_REQUEST,exception.getMessage());
        }
    }

    @Override
    public void update(QuizRequest quizRequest, Long id) throws Throwable {
        Quiz quiz = quizRepository.findById(id).orElseThrow(() ->{
            throw new EntityNotFoundException(Quiz.class,"id",id.toString());
        });
        quiz.setTitle(quizRequest.getTitle());
        quiz.setQuestion(quizRequest.getQuestion());
        quizRepository.save(quiz);

    }

    @Override
    public void delete(Long id) throws Throwable {
        try {
            Quiz quiz = quizRepository.findById(id).orElseThrow(() -> {
                throw new EntityNotFoundException(Quiz.class, "id", id.toString());
            });
            quizRepository.delete(quiz);
        } catch (Exception exception) {
            exception.printStackTrace();
            throw new BusinessException(ErrorCode.BAD_REQUEST, exception.getMessage());
        }
    }
}
