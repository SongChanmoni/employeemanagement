package com.example.exampleproject.service.homework;


import com.example.exampleproject.domain.homework.Homework;

import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.homework.HomeworkRequest;
import com.example.exampleproject.payload.pagination.PagingMainResponse;

import org.springframework.data.domain.Pageable;

public interface HomeworkService {
    ResponseData<?> getAll();

    PagingMainResponse<?> getAllWithPaging(Pageable pageable);

    ResponseData<?> getOne(Long id) throws Throwable;

    Homework insert(HomeworkRequest homeworkRequest) throws Throwable;

    void update(HomeworkRequest homeworkRequest, Long id) throws Throwable;

    void delete(Long id) throws Throwable;
}
