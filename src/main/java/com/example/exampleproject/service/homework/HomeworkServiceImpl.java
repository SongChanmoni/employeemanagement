package com.example.exampleproject.service.homework;

import com.example.exampleproject.domain.homework.Homework;
import com.example.exampleproject.domain.homework.HomeworkRepository;
import com.example.exampleproject.domain.user.User;
import com.example.exampleproject.enums.ErrorCode;
import com.example.exampleproject.exception.BusinessException;
import com.example.exampleproject.exception.EntityNotFoundException;
import com.example.exampleproject.payload.ResponseData;
import com.example.exampleproject.payload.homework.HomeworkRequest;
import com.example.exampleproject.payload.homework.HomeworkResponse;
import com.example.exampleproject.payload.pagination.PagingMainResponse;
import com.example.exampleproject.payload.user.UserResponse;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HomeworkServiceImpl implements HomeworkService{


    private HomeworkRepository homeworkRepository;

    @Autowired
    public void setHomeworkRepository(HomeworkRepository homeworkRepository) {this.homeworkRepository = homeworkRepository;}


    @Override
    public ResponseData<?> getAll() {
        List<Homework> homework = homeworkRepository.findAll();
        return new ResponseData<>(homework);
    }

    @Override
    public PagingMainResponse<?> getAllWithPaging(Pageable pageable) {
        Page<Homework> homeworkPage = homeworkRepository.findAll(pageable);
        return new PagingMainResponse<>(homeworkPage,homeworkPage.getContent());
     }

    @Override
    public ResponseData<?> getOne(Long id) throws Throwable {
        Homework homework = homeworkRepository.findById(id).orElseThrow(() -> {
            throw new EntityNotFoundException(User.class, "id", id.toString());
        });

        HomeworkResponse homeworkResponse= HomeworkResponse.builder()
                .title(homework.getTitle())
                .description(homework.getDescription())
                .build();
        return new ResponseData<>(homeworkResponse);
    }

    @Override
    public Homework insert(HomeworkRequest homeworkRequest) throws Throwable {
        Homework homework = homeworkRequest.toModel();
        try {
          homework = homeworkRepository.saveAndFlush(homework);
            return homework;
        }catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(ErrorCode.BAD_REQUEST, e.getMessage());
        }
    }

    @Override
    public void update(HomeworkRequest homeworkRequest, Long id) throws Throwable {
        Homework homework = homeworkRepository.findById(id).orElseThrow(()->{
            throw new EntityNotFoundException(User.class, "id", id.toString());
        });

        homework.setTitle(homeworkRequest.getTitle());
        homework.setDescription(homeworkRequest.getDescription());
        homeworkRepository.save(homework);

    }

    @Override
    public void delete(Long id) throws Throwable {
        try {

            Homework homework = homeworkRepository.findById(id).orElseThrow(() -> {
                throw new EntityNotFoundException(User.class, "id", id.toString());
            });
            homeworkRepository.delete(homework);

        }catch (Exception exception){
            exception.printStackTrace();
            throw new BusinessException(ErrorCode.BAD_REQUEST,exception.getMessage());
        }
    }
}
