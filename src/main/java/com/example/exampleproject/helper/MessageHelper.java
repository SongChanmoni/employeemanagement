package com.example.exampleproject.helper;
import org.apache.commons.lang3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component
public class MessageHelper {

    private static MessageSource source;

    @Autowired
    public void setSource(@Qualifier("messageSource") MessageSource source) {
        this.source = source;
    }

    static private Map<String, Locale> localeMap;

    static {
        localeMap = new HashMap<>();
        localeMap.put("km", new Locale("km", "KH"));
        localeMap.put("en", new Locale("en", "US"));
    }

    public static String getMessage(String key, Locale locale) {
        return getMessage(key, null, locale);
    }

    public static String getMessage( String key, String value, Locale locale) {

        try {
            return source.getMessage(key,null,locale);
        } catch (Exception e) {
//            AppLogManager.error(e);
        }

        return value;
    }

    public static Locale getLocale(HttpServletRequest request) {
        Locale locale = Locale.getDefault();

        String lang = null;

        if(request != null) {
            String acceptLang = request.getHeader("Accept-Language");

            if (StringUtils.isNotBlank(acceptLang)) {
                if (StringUtils.contains(acceptLang, ",")) {
                    lang = StringUtils.split(acceptLang, ",")[0];
                } else {
                    lang = acceptLang;
                }
            }
        }

        if(StringUtils.isNotBlank(lang)) {
            lang = normalizeLang(lang);

            if(localeMap.containsKey(lang)) {
                locale = localeMap.get(lang);
            }
        }

        return locale;
    }


    private static String normalizeLang(String lang) {
        try {
            if(lang.contains(",")) {
                lang = StringUtils.split(lang, ",")[0];
            }

            lang = lang.toLowerCase();
            return LocaleUtils.toLocale(StringUtils.replace(lang, "-", "_")).getLanguage();
        }
        catch(Exception e) {
            // AppLogManager.error(e);
//            AppLogManager.error("Request Lang format ",e.getMessage());
        }

        return lang;
    }

}