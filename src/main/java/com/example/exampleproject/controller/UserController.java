package com.example.exampleproject.controller;


import com.example.exampleproject.domain.user.User;
import com.example.exampleproject.domain.user.UserRepository;
import com.example.exampleproject.enums.ErrorCode;
import com.example.exampleproject.payload.user.UserRequest;
import com.example.exampleproject.service.user.UserServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

  UserRepository userRepository;
  // Initialize Service Implement
  private UserServiceImpl userService;

  @Autowired
  void setUserRepository(UserRepository userRepository) {
    this.userRepository = userRepository;
  }
  // inject User Service
  @Autowired
  public void setUserService(UserServiceImpl userService) {
    this.userService = userService;
  }
  // get all user
  @GetMapping
  public ResponseEntity<?> getAll() {
    try{
      return ResponseEntity.ok(userService.getAll());
    }catch (Exception sqlException) {
      Map<String, Object> map = new LinkedHashMap<>();
      map.put("status ", "false");
      map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
      System.out.println("SQL Exception : " + sqlException.getMessage());
      return ResponseEntity.ok().body(map);
    }
  }

  // get all user with pagination
  @GetMapping("/paging")
  public ResponseEntity<?> getAllWithPaging(
      @RequestParam(name = "page_number", defaultValue = "0") Integer pageNumber,
      @RequestParam(name = "page_size", defaultValue = "5") Integer pageSize) {
    try {
      Pageable pageable = PageRequest.of(pageNumber, pageSize);
      return ResponseEntity.ok(userService.getAllWithPaging(pageable));
    } catch (Exception sqlException) {
      Map<String, Object> map = new LinkedHashMap<>();
      map.put("status ", "false");
      map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
      System.out.println("SQL Exception : " + sqlException.getMessage());
      return ResponseEntity.ok().body(map);
    }
  }

  // get user by id
  @GetMapping("/{id}")
  public ResponseEntity<?> getOne(@PathVariable Long id) throws Throwable {
    try {
      return ResponseEntity.ok(userService.getOne(id));
    } catch (Exception ex) {
      ex.getMessage();
      return new ResponseEntity(ErrorCode.ITEM_NOT_FOUND.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

   //insert user
  @PostMapping
  public ResponseEntity<Map<String, Object>> insertUser(@Valid @RequestBody UserRequest userRequest) {
    try {
      Map<String, Object> map = new LinkedHashMap<>();
      User user1;
      try {
        user1 = userService.insert(userRequest);
        map.put("status", ErrorCode.POST_SUCCESS.getStatus());
        map.put("message", ErrorCode.POST_SUCCESS.getMessage());
        map.put("data", user1);
        return ResponseEntity.ok().body(map);
      } catch (Throwable e) {
        e.getMessage();
        return ResponseEntity.ok().build();
      }
    } catch (Exception sqlException) {
      Map<String, Object> map = new LinkedHashMap<>();
      map.put("status ", "false");
      map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
      System.out.println("SQL Exception : " + sqlException.getMessage());
      return ResponseEntity.ok().body(map);
    }
  }

  // update user
  @PutMapping("/{id}")
  ResponseEntity<?> update(@Valid @RequestBody UserRequest userRequest, @PathVariable Long id) throws Throwable {
    try {
      Map<String, Object> map = new LinkedHashMap<>();
      try {
        userService.update(userRequest, id);
        map.put("status", ErrorCode.UPDATED_SUCCESS.getStatus());
        map.put("message", ErrorCode.UPDATED_SUCCESS.getMessage());
        map.put("data", userService.getOne(id));
        return ResponseEntity.ok().body(map);
      } catch (Exception e) {
        e.getMessage();
        return ResponseEntity.ok().build();
      }
    } catch (Exception sqlException) {
      Map<String, Object> map = new LinkedHashMap<>();
      map.put("status ", "false");
      map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
      System.out.println("SQL Exception : " + sqlException.getMessage());
      return ResponseEntity.ok().body(map);
    }
  }

  // delete user
  @DeleteMapping("/{id}")
  public ResponseEntity<?> delete(@PathVariable Long id) throws Throwable {
    try {
      Map<String, Object> map = new LinkedHashMap<>();
      Optional<User> newUser = userRepository.findById(id);

      if (newUser.isEmpty()) {
        map.put("status", ErrorCode.ITEM_NOT_FOUND.getStatus());
        map.put("message", ErrorCode.ITEM_NOT_FOUND.getMessage());
        return ResponseEntity.ok().body(map);
      }

      try {
        userService.delete(id);
        map.put("status", ErrorCode.DELETED_SUCCESS.getStatus());
        map.put("message", ErrorCode.DELETED_SUCCESS.getMessage());
        return ResponseEntity.ok().body(map);
      } catch (Exception e) {
        e.getMessage();
        return ResponseEntity.ok().build();
      }
    } catch (Exception sqlException) {
      Map<String, Object> map = new LinkedHashMap<>();
      map.put("status ", "false");
      map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
      System.out.println("SQL Exception : " + sqlException.getMessage());
      return ResponseEntity.ok().body(map);
    }
  }

//  @GetMapping("/filter")
//  public ResponseEntity<?> filterUsername(Model model, @Param("keyword") String keyword) {
//    try {
//      List<User> listUsers = userService.listAll(keyword);
//      return ResponseEntity.ok(listUsers);
//    } catch (Exception sqlException) {
//      Map<String, Object> map = new LinkedHashMap<>();
//      map.put("status ", "false");
//      map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
//      System.out.println("SQL Exception : " + sqlException.getMessage());
//      return ResponseEntity.ok().body(map);
//    }
//  }
}
