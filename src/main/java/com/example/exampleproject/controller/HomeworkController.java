package com.example.exampleproject.controller;


import com.example.exampleproject.domain.homework.Homework;
import com.example.exampleproject.domain.homework.HomeworkRepository;
import com.example.exampleproject.domain.user.User;
import com.example.exampleproject.enums.ErrorCode;
import com.example.exampleproject.payload.homework.HomeworkRequest;
import com.example.exampleproject.payload.user.UserRequest;
import com.example.exampleproject.service.homework.HomeworkServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/homeworks")
public class HomeworkController {

    private HomeworkRepository homeworkRepository;
    private HomeworkServiceImpl homeworkServiceImpl;

    @Autowired
    public void setHomeworkRepository(HomeworkRepository homeworkRepository) {
        this.homeworkRepository = homeworkRepository;
    }

    @Autowired
    public void setHomeworkServiceImpl(HomeworkServiceImpl homeworkServiceImpl) {
        this.homeworkServiceImpl = homeworkServiceImpl;
    }

    //get all homework
    @GetMapping
    public ResponseEntity<?> getAll() {
        try{
            return ResponseEntity.ok(homeworkServiceImpl.getAll());
        }catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }

    // get all homework with pagination
    @GetMapping("/paging")
    public ResponseEntity<?> getAllWithPaging(
            @RequestParam(name = "page_number", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "page_size", defaultValue = "5") Integer pageSize) {
        try {
            Pageable pageable = PageRequest.of(pageNumber, pageSize);
            return ResponseEntity.ok(homeworkServiceImpl.getAllWithPaging(pageable));
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }

    // get user by id
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Long id) throws Throwable {
        try {
            return ResponseEntity.ok(homeworkServiceImpl.getOne(id));
        } catch (Exception ex) {
            ex.getMessage();
            return new ResponseEntity(ErrorCode.ITEM_NOT_FOUND.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }



    //insert homework
    @PostMapping
    public ResponseEntity<Map<String, Object>> insert(@Valid @RequestBody HomeworkRequest homeworkRequest) {
        try {
            Map<String, Object> map = new LinkedHashMap<>();
            Homework homework;
            try {
                homework = homeworkServiceImpl.insert(homeworkRequest);
                map.put("status", ErrorCode.POST_SUCCESS.getStatus());
                map.put("message", ErrorCode.POST_SUCCESS.getMessage());
                map.put("data", homework);
                return ResponseEntity.ok().body(map);
            } catch (Throwable e) {
                e.getMessage();
                return ResponseEntity.ok().build();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


    // update homework
    @PutMapping("/{id}")
    ResponseEntity<?> update(@Valid @RequestBody HomeworkRequest homeworkRequest, @PathVariable Long id) throws Throwable {
        try {
            Map<String, Object> map = new LinkedHashMap<>();
            try {
                homeworkServiceImpl.update(homeworkRequest,id);
                map.put("status", ErrorCode.UPDATED_SUCCESS.getStatus());
                map.put("message", ErrorCode.UPDATED_SUCCESS.getMessage());
                map.put("data", homeworkServiceImpl.getOne(id));
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.getMessage();
                return ResponseEntity.ok().build();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }

    // delete homework
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) throws Throwable {
        try {
            Map<String, Object> map = new LinkedHashMap<>();
            Optional<Homework> optionalHomework = homeworkRepository.findById(id);

            if (optionalHomework.isEmpty()) {
                map.put("status", ErrorCode.ITEM_NOT_FOUND.getStatus());
                map.put("message", ErrorCode.ITEM_NOT_FOUND.getMessage());
                return ResponseEntity.ok().body(map);
            }

            try {
                homeworkServiceImpl.delete(id);
                map.put("status", ErrorCode.DELETED_SUCCESS.getStatus());
                map.put("message", ErrorCode.DELETED_SUCCESS.getMessage());
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.getMessage();
                return ResponseEntity.ok().build();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }

}
