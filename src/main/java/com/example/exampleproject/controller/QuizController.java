package com.example.exampleproject.controller;


import com.example.exampleproject.domain.quiz.Quiz;
import com.example.exampleproject.domain.quiz.QuizRepository;
import com.example.exampleproject.enums.ErrorCode;
import com.example.exampleproject.payload.quiz.QuizRequest;
import com.example.exampleproject.service.quiz.QuizServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/quizzes")
public class QuizController {

    private QuizRepository quizRepository;
    private QuizServiceImpl quizServiceImpl;

    @Autowired
    public void setQuizRepository(QuizRepository quizRepository) {this.quizRepository = quizRepository;}

    @Autowired
    public void setQuizServiceImpl(QuizServiceImpl quizServiceImpl) {this.quizServiceImpl = quizServiceImpl;}

    //get all quiz
    @GetMapping
    public ResponseEntity<?> getAll(){
        try {
            return ResponseEntity.ok(quizServiceImpl.getAll());

        }catch (Exception exception){
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("status","false");
            map.put("message", ErrorCode.BAD_REQUEST.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }

    //insert quiz
    @PostMapping
    public ResponseEntity<Map<String, Object>> insert(@Valid @RequestBody QuizRequest quizRequest) {
        try {
            try {
                Map<String, Object> map = new LinkedHashMap<>();
                Quiz quiz;
                quiz = quizServiceImpl.insert(quizRequest);
                map.put("status ", ErrorCode.POST_SUCCESS.getStatus());
                map.put("message", ErrorCode.POST_SUCCESS.getMessage());
                map.put("data", quiz);
                return ResponseEntity.ok().body(map);
            } catch (Throwable e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


    //get quiz by id
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Long id) throws Throwable {
        try {
            return ResponseEntity.ok(quizServiceImpl.getOne(id));
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(ErrorCode.ITEM_NOT_FOUND.getStatus(), HttpStatus.NOT_FOUND);
        }
    }

    //get employee with paging
    @GetMapping("/paging")
    public ResponseEntity<?> getAllWithPaging(
            @RequestParam(name = "page_number", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "page_size", defaultValue = "5") Integer pageSize) {
        try {
            Pageable pageable = PageRequest.of(pageNumber, pageSize);
            return ResponseEntity.ok(quizServiceImpl.getAllWithPaging(pageable));
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.BAD_REQUEST.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


    //update quiz
    @PutMapping("/{id}")
    ResponseEntity<?> update(@Valid @RequestBody QuizRequest quizRequest, @PathVariable Long id) throws Throwable {
        try {
            Map<String, Object> map = new LinkedHashMap<>();
            try {
                quizServiceImpl.update(quizRequest,id);
                map.put("status", ErrorCode.UPDATED_SUCCESS.getStatus());
                map.put("message", ErrorCode.UPDATED_SUCCESS.getMessage());
                map.put("data", quizServiceImpl.getOne(id));
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }
//
//        try {
//            Map<String, Object> map = new LinkedHashMap<>();
//                quizServiceImpl.update(quizRequest,id);
//                map.put("status", ErrorCode.UPDATED_SUCCESS.getStatus());
//                map.put("message", ErrorCode.UPDATED_SUCCESS.getMessage());
//                map.put("data",quizServiceImpl.getOne(id));
//                return ResponseEntity.ok().body(map);
//        } catch (Exception sqlException) {
//            Map<String, Object> map = new LinkedHashMap<>();
//            map.put("status ", "false");
//            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
//            System.out.println("SQL Exception : " + sqlException.getMessage());
//            return ResponseEntity.ok().body(map);
//        }
//    }


    //delete quiz
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) throws Throwable {

        try {
            Map<String, Object> map = new LinkedHashMap<>();
            Optional<Quiz> quizOptional = quizRepository.findById(id);
            if (quizOptional.isEmpty()) {
                map.put("message", ErrorCode.ITEM_NOT_FOUND.getMessage());
                map.put("status", ErrorCode.ITEM_NOT_FOUND.getStatus());
                return ResponseEntity.ok().body(map);
            }
            try {
                quizServiceImpl.delete(id);
                map.put("status", ErrorCode.DELETED_SUCCESS.getStatus());
                map.put("message", ErrorCode.DELETED_SUCCESS.getMessage());
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }

}
