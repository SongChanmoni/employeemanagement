package com.example.exampleproject.controller;

import com.example.exampleproject.domain.student.Student;
import com.example.exampleproject.domain.student.StudentRepository;
import com.example.exampleproject.domain.user.User;
import com.example.exampleproject.enums.ErrorCode;
import com.example.exampleproject.payload.student.StudentRequest;
import com.example.exampleproject.service.student.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/students")
public class StudentController {

    private StudentServiceImpl studentServiceImp;
    private StudentRepository studentRepository;

    @Autowired
    void setStudentRepository(StudentRepository studentRepository){
        this.studentRepository= studentRepository;
    }

    @Autowired
   void setStudentServiceImp(StudentServiceImpl studentServiceImp){this.studentServiceImp = studentServiceImp;}


    //get all student
    @GetMapping
    public ResponseEntity<?> getAll(){
        try {
            try {
                return ResponseEntity.ok(studentServiceImp.getAll());
            }catch (Exception exception){
                return new ResponseEntity<>(ErrorCode.BAD_REQUEST.getStatus(), HttpStatus.NOT_FOUND);
            }

        }catch (Exception exception){
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("status","false");
            map.put("message",ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);

        }
    }

    //get student by id
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Long id) throws Throwable {
        try {
            return ResponseEntity.ok(studentServiceImp.getOne(id));
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(ErrorCode.ITEM_NOT_FOUND.getStatus(), HttpStatus.NOT_FOUND);
        }
    }


    //get student with paging
    @GetMapping("/paging")
    public ResponseEntity<?> getAllWithPaging(
            @RequestParam(name = "page_number", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "page_size", defaultValue = "5") Integer pageSize) {
        try {
            Pageable pageable = PageRequest.of(pageNumber, pageSize);
            return ResponseEntity.ok(studentServiceImp.getAllWithPaging(pageable));
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


    //insert student
    @PostMapping
    public ResponseEntity<Map<String, Object>> insert(@Valid @RequestBody StudentRequest studentRequest) {
        try {
            try {
                Map<String, Object> map = new LinkedHashMap<>();
                Student student;
                student = studentServiceImp.insert(studentRequest);
                map.put("status ", ErrorCode.POST_SUCCESS.getStatus());
                map.put("message", ErrorCode.POST_SUCCESS.getMessage());
                map.put("data", student);
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);
        }
        return null;
    }


    //update student
    @PutMapping("/{id}")
    ResponseEntity<?> update(@Valid @RequestBody StudentRequest studentRequest, @PathVariable Long id) throws Throwable {
        try {
            Map<String, Object> map = new LinkedHashMap<>();
            try {
                studentServiceImp.update(studentRequest,id);
                map.put("status", ErrorCode.UPDATED_SUCCESS.getStatus());
                map.put("message", ErrorCode.UPDATED_SUCCESS.getMessage());
                map.put("data", studentServiceImp.getOne(id));
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


    //delete student
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) throws Throwable {
        try {
            Map<String, Object> map = new LinkedHashMap<>();
            Optional<Student> student = studentRepository.findById(id);
            if (student.isEmpty()) {
                map.put("message", ErrorCode.ITEM_NOT_FOUND.getMessage());
                map.put("status", ErrorCode.ITEM_NOT_FOUND.getStatus());
                return ResponseEntity.ok().body(map);
            }
            try {
                studentServiceImp.delete(id);
                map.put("status", ErrorCode.DELETED_SUCCESS.getStatus());
                map.put("message", ErrorCode.DELETED_SUCCESS.getMessage());
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }

    //filter student
    @GetMapping("/filter")
    public ResponseEntity<?> filterStudentName(Model model, @Param("name") String name) {
        try {
            List<Student> studentList = studentServiceImp.findStudentByName(name);
            return ResponseEntity.ok(studentList);
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


}
