package com.example.exampleproject.controller;

import com.example.exampleproject.component.jwtConfig.AuthenticationProvider;
import com.example.exampleproject.configuration.jwt.JwtTokenUtil;
import com.example.exampleproject.payload.LoginRequest;
import com.example.exampleproject.payload.baseresponse.SingleResult;
import com.example.exampleproject.security.UserServiceDetails;
import com.example.exampleproject.service.ResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @Autowired
    private AuthenticationProvider authenticationProvider;

    @Autowired
    private UserServiceDetails userService;

    @Autowired
    private ResponseService responseService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    @PostMapping("/api/v1/login")
    public SingleResult login(@RequestBody LoginRequest payload) throws Exception{
        try {
            authenticationProvider.authenticate(payload.getUsername(),payload.getPassword());

             UserDetails userDetails = userService.loadUserByUsername(payload.getUsername());

            return responseService.getSingleResult(jwtTokenUtil.generateToken(userDetails));

        }catch (Exception e){
            throw new RuntimeException("Incorrect username or password");
        }

    }

}
