package com.example.exampleproject.controller;

import com.example.exampleproject.domain.teacher.Teacher;
import com.example.exampleproject.domain.teacher.TeacherRepository;
import com.example.exampleproject.domain.user.User;
import com.example.exampleproject.enums.ErrorCode;
import com.example.exampleproject.payload.teacher.TeacherRequest;
import com.example.exampleproject.service.teacher.TeacherServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/teachers")
public class TeacherController {

    private TeacherServiceImpl teacherServiceImp;
    private TeacherRepository teacherRepository;
    @Autowired
    void setTeacherServiceImp(TeacherServiceImpl teacherServiceImp){
        this.teacherServiceImp = teacherServiceImp;
    }

    @Autowired
    void setTeacherRepository(TeacherRepository teacherRepository){
        this.teacherRepository = teacherRepository;
    }


    //get all teacher
    @GetMapping
    public ResponseEntity<?> getAll(){
        try {
            try {
                return ResponseEntity.ok(teacherServiceImp.getAll());
            }catch (Exception exception){
                return new ResponseEntity<>(ErrorCode.BAD_REQUEST.getStatus(), HttpStatus.NOT_FOUND);
            }

        }catch (Exception exception){
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("status","false");
            map.put("message",ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


    //get teacher by id
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Long id) throws Throwable {

        try {
            return ResponseEntity.ok(teacherServiceImp.getOne(id));
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(ErrorCode.ITEM_NOT_FOUND.getStatus(), HttpStatus.NOT_FOUND);
        }
    }


    //get teacher with paging
    @GetMapping("/paging")
    public ResponseEntity<?> getAllWithPaging(
            @RequestParam(name = "page_number", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "page_size", defaultValue = "5") Integer pageSize) {
        try {
            Pageable pageable = PageRequest.of(pageNumber, pageSize);
            return ResponseEntity.ok(teacherServiceImp.getAllWithPaging((org.springdoc.core.converters.models.Pageable) pageable));
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


    //insert teacher
    @PostMapping
    public ResponseEntity<Map<String, Object>> insert(@Valid @RequestBody TeacherRequest teacherRequest) {
        try {

            try {
                Map<String, Object> map = new LinkedHashMap<>();
                Teacher teacher;
                teacher = teacherServiceImp.insert(teacherRequest);
                map.put("status ", ErrorCode.POST_SUCCESS.getStatus());
                map.put("message", ErrorCode.POST_SUCCESS.getMessage());
                map.put("data", teacher);
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
        return null;
    }


    //update teacher
    @PutMapping("/{id}")
    ResponseEntity<?> update(@Valid @RequestBody TeacherRequest teacherRequest, @PathVariable Long id) throws Throwable {

        try {
            Map<String, Object> map = new LinkedHashMap<>();
            try {
                teacherServiceImp.update(teacherRequest,id);
                map.put("status", ErrorCode.UPDATED_SUCCESS.getStatus());
                map.put("message", ErrorCode.UPDATED_SUCCESS.getMessage());
                map.put("data",teacherServiceImp.getOne(id));
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


    //delete teacher
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) throws Throwable {

        try {
            Map<String, Object> map = new LinkedHashMap<>();
            Optional<Teacher> teacher = teacherRepository.findById(id);
            if (teacher.isEmpty()) {
                map.put("message", ErrorCode.ITEM_NOT_FOUND.getMessage());
                map.put("status", ErrorCode.ITEM_NOT_FOUND.getStatus());
                return ResponseEntity.ok().body(map);
            }
            try {
                teacherServiceImp.delete(id);
                map.put("status", ErrorCode.DELETED_SUCCESS.getStatus());
                map.put("message", ErrorCode.DELETED_SUCCESS.getMessage());
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


    //filter teacher
    @GetMapping("/filter")
    public ResponseEntity<?> filterTeacher(Model model, @Param("name") String name) {
        try {
            List<Teacher> teacherList = teacherServiceImp.findTeacherByName(name);
            return ResponseEntity.ok(teacherList);
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }

}
