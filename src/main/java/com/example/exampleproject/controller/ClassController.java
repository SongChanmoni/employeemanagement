package com.example.exampleproject.controller;


import com.example.exampleproject.domain.classdomain.Class;
import com.example.exampleproject.domain.classdomain.ClassRepository;
import com.example.exampleproject.domain.user.User;
import com.example.exampleproject.enums.ErrorCode;
import com.example.exampleproject.payload.classpayload.ClassRequest;
import com.example.exampleproject.service.classservice.ClassServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/classes")
public class ClassController {

    private ClassRepository classRepository;
    private ClassServiceImpl classServiceIml;

    @Autowired
    public void setClassRepository(ClassRepository classRepository) {this.classRepository = classRepository;}

    @Autowired
    public void setClassServiceIml(ClassServiceImpl classServiceIml) {this.classServiceIml = classServiceIml;}

    //get all class
    @GetMapping
    public ResponseEntity<?> getAll(){
        try {
            try {
                return ResponseEntity.ok(classServiceIml.getAll());
            }catch (Exception exception){
                return new ResponseEntity<>(ErrorCode.BAD_REQUEST.getStatus(), HttpStatus.NOT_FOUND);
            }

        }catch (Exception exception){
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("status","false");
            map.put("message",ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);

        }
    }

    //get class by id
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Long id) throws Throwable {
        try {
            return ResponseEntity.ok(classServiceIml.getOne(id));
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(ErrorCode.ITEM_NOT_FOUND.getStatus(), HttpStatus.NOT_FOUND);
        }
    }


    //get class with paging
    @GetMapping("/paging")
    public ResponseEntity<?> getAllWithPaging(
            @RequestParam(name = "page_number", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "page_size", defaultValue = "5") Integer pageSize) {
        try {
            Pageable pageable = PageRequest.of(pageNumber, pageSize);
            return ResponseEntity.ok(classServiceIml.getAllWithPaging(pageable));
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


    //insert lass
    @PostMapping
    public ResponseEntity<Map<String, Object>> insert(@Valid @RequestBody ClassRequest classRequest) {
        try {
            try {
                Map<String, Object> map = new LinkedHashMap<>();
                Class aClass;
                aClass = classServiceIml.insert(classRequest);
                map.put("status ", ErrorCode.POST_SUCCESS.getStatus());
                map.put("message", ErrorCode.POST_SUCCESS.getMessage());
                map.put("data", aClass);
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);
        }
        return null;
    }


    //update class
    @PutMapping("/{id}")
    ResponseEntity<?> update(@Valid @RequestBody ClassRequest classRequest, @PathVariable Long id) throws Throwable {
        try {
            Map<String, Object> map = new LinkedHashMap<>();
            try {
                classServiceIml.update(classRequest,id);
                map.put("status", ErrorCode.UPDATED_SUCCESS.getStatus());
                map.put("message", ErrorCode.UPDATED_SUCCESS.getMessage());
                map.put("data", classServiceIml.getOne(id));
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


    //delete class
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) throws Throwable {
        try {
            Map<String, Object> map = new LinkedHashMap<>();
            Optional<Class> classOptional = classRepository.findById(id);
            if (classOptional.isEmpty()) {
                map.put("message", ErrorCode.ITEM_NOT_FOUND.getMessage());
                map.put("status", ErrorCode.ITEM_NOT_FOUND.getStatus());
                return ResponseEntity.ok().body(map);
            }
            try {
                classServiceIml.delete(id);
                map.put("status", ErrorCode.DELETED_SUCCESS.getStatus());
                map.put("message", ErrorCode.DELETED_SUCCESS.getMessage());
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


    //filter class
    @GetMapping("/filter")
    public ResponseEntity<?> filterClassName(Model model, @Param("name") String name) {
        try {
            List<Class> classList = classServiceIml.findClassByName(name);
            return ResponseEntity.ok(classList);
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }

}
