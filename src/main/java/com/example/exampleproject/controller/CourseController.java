package com.example.exampleproject.controller;


import com.example.exampleproject.domain.course.Course;
import com.example.exampleproject.domain.course.CourseRepository;
import com.example.exampleproject.domain.student.Student;
import com.example.exampleproject.domain.user.User;
import com.example.exampleproject.enums.ErrorCode;
import com.example.exampleproject.payload.course.CourseRequest;
import com.example.exampleproject.payload.student.StudentRequest;

import com.example.exampleproject.service.course.CourseServiceImpI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/courses")
public class CourseController {

    private CourseRepository courseRepository;
    private CourseServiceImpI courseServiceImpI;

    @Autowired
    public void setCourseRepository(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Autowired
    public void setCourseServiceImpI(CourseServiceImpI courseServiceImpI) {this.courseServiceImpI = courseServiceImpI;}


    //get all course
    @GetMapping
    public ResponseEntity<?> getAll(){
        try {
            try {
                return ResponseEntity.ok(courseServiceImpI.getAll());
            }catch (Exception exception){
                return new ResponseEntity<>(ErrorCode.BAD_REQUEST.getStatus(), HttpStatus.NOT_FOUND);
            }

        }catch (Exception exception){
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("status","false");
            map.put("message",ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


    //get course by id
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable Long id) throws Throwable {
        try {
            return ResponseEntity.ok(courseServiceImpI.getOne(id));
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(ErrorCode.ITEM_NOT_FOUND.getStatus(), HttpStatus.NOT_FOUND);
        }
    }


    //get course with paging
    @GetMapping("/paging")
    public ResponseEntity<?> getAllWithPaging(
            @RequestParam(name = "page_number", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "page_size", defaultValue = "5") Integer pageSize) {
        try {
            Pageable pageable = PageRequest.of(pageNumber, pageSize);
            return ResponseEntity.ok(courseServiceImpI.getAllWithPaging((org.springdoc.core.converters.models.Pageable) pageable));
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }

    //insert course
    @PostMapping
    public ResponseEntity<Map<String, Object>> insert(@Valid @RequestBody CourseRequest courseRequest) {
        try {

            try {
                Map<String, Object> map = new LinkedHashMap<>();
                Course course;
                course = courseServiceImpI.insert(courseRequest);
                map.put("status ", ErrorCode.POST_SUCCESS.getStatus());
                map.put("message", ErrorCode.POST_SUCCESS.getMessage());
                map.put("data", course);
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);
        }
        return null;
    }


    //update course
    @PutMapping("/{id}")
    ResponseEntity<?> update(@Valid @RequestBody CourseRequest  courseRequest, @PathVariable Long id) throws Throwable {
        try {
            Map<String, Object> map = new LinkedHashMap<>();
            try {
                courseServiceImpI.update(courseRequest,id);
                map.put("status", ErrorCode.UPDATED_SUCCESS.getStatus());
                map.put("message", ErrorCode.UPDATED_SUCCESS.getMessage());
                map.put("data", courseServiceImpI.getOne(id));
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }


    //delete course
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id)
            throws Throwable {

        try {
            Map<String, Object> map = new LinkedHashMap<>();
            Optional<Course> course = courseRepository.findById(id);
            if (course.isEmpty()) {
                map.put("message", ErrorCode.ITEM_NOT_FOUND.getMessage());
                map.put("status", ErrorCode.ITEM_NOT_FOUND.getStatus());
                return ResponseEntity.ok().body(map);
            }
            try {
                courseServiceImpI.delete(id);
                map.put("status", ErrorCode.DELETED_SUCCESS.getStatus());
                map.put("message", ErrorCode.DELETED_SUCCESS.getMessage());
                return ResponseEntity.ok().body(map);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.ok().build();
            }
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }

    //filter course
    @GetMapping("/filter")
    public ResponseEntity<?> filterCourseTitle(Model model, @Param("Title") String title) {
        try {
            List<Course> courseList = courseServiceImpI.findCourseByTitle(title);
            return ResponseEntity.ok(courseList);
        } catch (Exception sqlException) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("status ", "false");
            map.put("message ", ErrorCode.EXCEPTION_ACCORS.getMessage());
            System.out.println("SQL Exception : " + sqlException.getMessage());
            return ResponseEntity.ok().body(map);
        }
    }

}
