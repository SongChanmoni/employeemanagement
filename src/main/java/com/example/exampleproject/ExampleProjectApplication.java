package com.example.exampleproject;

import com.example.exampleproject.domain.user.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication

public class ExampleProjectApplication {

    public static void main(String[] args) {

        SpringApplication.run(ExampleProjectApplication.class, args);
    }

}
