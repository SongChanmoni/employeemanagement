package com.example.exampleproject.domain.teacher;


import com.example.exampleproject.domain.course.Course;
import com.example.exampleproject.domain.quiz.Quiz;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity(name = "sd_teacher")
@Data
@NoArgsConstructor
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String gender;

    @Builder
    public Teacher(Long id, String name, String gender) {
        this.id = id;
        this.name = name;
        this.gender = gender;
    }

    @OneToMany(mappedBy = "teacher")
    private List<Course> courses;

}
