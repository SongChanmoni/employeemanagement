package com.example.exampleproject.domain.teacher;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TeacherRepository extends JpaRepository<Teacher,Long> {

    @Query("select s from sd_teacher s where upper(s.name) = upper(?1) ")
    List<Teacher> findTeacherByName(String name);




}
