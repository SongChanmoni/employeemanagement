package com.example.exampleproject.domain.course;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course,Long> {

    @Query("select s from sd_course s where upper(s.title) = upper(?1)")
    List<Course> findCourseByTitle(String title);


}
