package com.example.exampleproject.domain.course;


import com.example.exampleproject.domain.quiz.Quiz;
import com.example.exampleproject.domain.student.Student;
import com.example.exampleproject.domain.teacher.Teacher;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonMerge;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity(name = "sd_course")
@Data
@NoArgsConstructor
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;

    @Builder
    public Course(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    //Relationship With Teacher
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "TEACHER_ID", referencedColumnName = "ID")
    private Teacher teacher;

    //Relationship With Student
    @ManyToMany
    @JsonManagedReference
    @JoinTable(name = "STUDENTS_COURSES", joinColumns = @JoinColumn(name = "COURSE_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "STUDENT_ID", referencedColumnName = "ID"))
    private List<Student> students;

    @OneToMany
    private List<Quiz> quizzes;
}
