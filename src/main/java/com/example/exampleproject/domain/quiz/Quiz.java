package com.example.exampleproject.domain.quiz;


import com.example.exampleproject.domain.classdomain.Class;
import com.example.exampleproject.domain.course.Course;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "sd_quiz")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Quiz {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String question;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "COURSE_ID", referencedColumnName = "id")
    private Course course;


}
