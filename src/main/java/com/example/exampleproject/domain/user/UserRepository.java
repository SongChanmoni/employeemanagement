package com.example.exampleproject.domain.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

//    @Query("SELECT u FROM sd_user u WHERE CONCAT(u.lastname, ' ', u.firstname, ' ', u.username, ' ') LIKE %?1%")
//    List<User> search(String keyword);

    Optional<User> findByUsername(String username);


}
