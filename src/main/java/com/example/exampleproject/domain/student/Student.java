package com.example.exampleproject.domain.student;

import com.example.exampleproject.domain.classdomain.Class;
import com.example.exampleproject.domain.course.Course;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity(name = "sd_student")
@Data
@NoArgsConstructor
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String gender;


    @Builder
    public Student(Long id, String name, String gender) {
        this.id = id;
        this.name = name;
        this.gender = gender;
    }

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "CLASS_ID", referencedColumnName = "id")
    private Class aClass;

}
