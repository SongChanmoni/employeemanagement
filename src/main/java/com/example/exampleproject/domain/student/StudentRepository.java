package com.example.exampleproject.domain.student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,Long> {
    @Query("select s from sd_student s where upper(s.name) = upper(?1)")
    List<Student> findStudentByName(String name);


}
