package com.example.exampleproject.domain.classdomain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ClassRepository extends JpaRepository<Class,Long> {
    @Query("select s from sd_class s where upper(s.name) = upper(?1)")
    List<Class> findClassByName(String name);


}
