package com.example.exampleproject.domain.classdomain;


import com.example.exampleproject.domain.course.Course;
import com.example.exampleproject.domain.student.Student;
import com.example.exampleproject.domain.teacher.Teacher;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonMerge;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity(name = "sd_class")
@Data
@NoArgsConstructor
public class Class {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @Builder
    public Class(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    //Relationship With Student
    @OneToMany()
    private List<Student> students;



}
