package com.example.exampleproject.payload.pagination;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.List;

@Getter
@Setter
public class PagingMainResponse<T> extends Pagination {

    private List<T> response;

    public PagingMainResponse(Page<?> page, List<T> response){
        super(page);
        this.response = response;
    }
}