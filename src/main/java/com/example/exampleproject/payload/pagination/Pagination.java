package com.example.exampleproject.payload.pagination;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

@Getter
@Setter
public class Pagination {

    @JsonProperty("last_page")
    private Integer lastPage;

    private boolean last;

    @JsonProperty("total_pages")
    private Integer totalPages;

    @JsonProperty("total_elements")
    private Long totalElements;

    @JsonProperty("number_elements")
    private Integer numberOfElements;

    private boolean first;

    private Integer size;

    private Integer number;

    private boolean empty;

    public Pagination(Page<?> page) {

        this.last = page.isLast();
        this.totalPages = page.getTotalPages();
        this.totalElements = page.getTotalElements();
        this.numberOfElements = page.getNumber();
        this.first = page.isFirst();
        this.size = page.getSize();
        this.number = page.getNumber();
        this.empty = page.isEmpty();
    }
}
