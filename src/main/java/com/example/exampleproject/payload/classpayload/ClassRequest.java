package com.example.exampleproject.payload.classpayload;

import com.example.exampleproject.domain.classdomain.Class;
import lombok.Getter;

import java.util.List;

@Getter
public class ClassRequest {
    private String name;
    private List<Long> studentId;


    public Class toModel(){
        return Class.builder()
                .name(name)
                .build();
    }
}
