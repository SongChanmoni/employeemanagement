package com.example.exampleproject.payload.homework;

import com.example.exampleproject.domain.homework.Homework;
import lombok.Data;

@Data
public class HomeworkRequest {
    private String title;
    private String description;

    public Homework toModel(){
        return Homework.builder()
                .title(title)
                .description(description)
                .build();
    }
}
