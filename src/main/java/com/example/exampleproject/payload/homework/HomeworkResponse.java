package com.example.exampleproject.payload.homework;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.batch.BatchDataSource;

@Builder
@NoArgsConstructor
@Data
@AllArgsConstructor
public class HomeworkResponse {

    private Long id;
    private String title;
    private String description;
}
