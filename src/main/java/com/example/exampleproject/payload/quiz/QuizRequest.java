package com.example.exampleproject.payload.quiz;

import com.example.exampleproject.domain.quiz.Quiz;
import lombok.Data;

@Data
public class QuizRequest {

    private String title;
    private String question;

    public Quiz toModel(){
        return Quiz.builder()
                .title(title)
                .question(question)
                .build();
    }
}
