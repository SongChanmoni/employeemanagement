package com.example.exampleproject.payload.course;

import com.example.exampleproject.domain.course.Course;
import com.example.exampleproject.domain.student.Student;
import lombok.Getter;

import java.util.List;

@Getter
public class CourseRequest {

    private String title;
    private Long teacherId;
    private List<Long> studentId;
    private List<Long> quizId;

    public Course toModel(){
        return Course.builder()
                .title(title)
                .build();
    }

}
