package com.example.exampleproject.payload.teacher;

import com.example.exampleproject.domain.teacher.Teacher;
import lombok.Getter;

import java.util.List;

@Getter
public class TeacherRequest {

    private String name;
    private String gender;

    public Teacher toModel(){
        return Teacher.builder()
                .name(name)
                .gender(gender)
                .build();
    }
}
