package com.example.exampleproject.payload.error;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ApiResponse <T>{
    private ApiError error;

    public ApiResponse(ApiError error) {
        super();
        this.error = error;
    }

}