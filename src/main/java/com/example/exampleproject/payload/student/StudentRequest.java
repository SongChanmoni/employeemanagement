package com.example.exampleproject.payload.student;

import com.example.exampleproject.domain.student.Student;
import lombok.Getter;

@Getter
public class StudentRequest {

    private Long classId;
    private String name;
    private String gender;

    public Student toModel(){
        return Student.builder()
                .name(name)
                .gender(gender)
                .build();
    }
}
