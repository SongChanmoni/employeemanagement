package com.example.exampleproject.payload.user;


import com.example.exampleproject.domain.user.User;
import com.example.exampleproject.domain.user.UserRole;
import lombok.Data;

import java.util.Collections;

@Data
public class UserRequest {

    private String firstname;
    private String lastname;
    private String username;
    private String password;
    private String role;
    public User toModel() {
        return User.builder()
                .firstname(firstname)
                .lastname(lastname)
                .username(username)
                .password(password)
                .roles(Collections.singleton(UserRole.valueOf(role)))
                .build();
    }
}
